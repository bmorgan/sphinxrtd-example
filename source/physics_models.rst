.. _physics_models:

Physics Models
--------------

This is a simple doc to check if inline LaTeX and MathJax work.
Some basic examples taken from `Sphinx's manual <http://www.sphinx-doc.org/en/stable/ext/math.html>`_
are implemented below.

Inline Math
~~~~~~~~~~~

Since Pythagoras, we know that :math:`a^2 + b^2 = c^2`.

Using the Math Directive(s)
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Multiple equations:

.. math::

   (a + b)^2 = a^2 + 2ab + b^2

   (a - b)^2 = a^2 - 2ab + b^2


Same set, but self managed:

.. math::
   :nowrap:

   \begin{eqnarray}
      y    & = & ax^2 + bx + c \\
      f(x) & = & x^2 + 2xy + y^2
   \end{eqnarray}

References
~~~~~~~~~~

.. math:: e^{i\pi} + 1 = 0
   :label: euler

Euler's identity, equation :eq:`euler`, was elected one of the most
beautiful mathematical formulas.
