FAQ
---

This is an example of using a Markdown document within Sphinx. It
uses the [recommonmark](https://github.com/rtfd/recommonmark) bridge
between the [CommonMark](http://commonmark.org) Markdown specification
and Sphinx. In addition, the [Pandoc](https://github.com/jgm/pandoc)
markup converter supports this particular Markdown spec.

Markdown is not a fully featured as reStructuredText, but is good for
use cases where only plain text is required.

Here's an example list structure from the CommonMark spec

1. List item one.

   List item one continued with a second paragraph followed by an
   Indented block.

       $ ls *.sh
       $ mv *.sh ~/tmp

   List item continued with a third paragraph.

2. List item two continued with an open block.

   This paragraph is part of the preceding list item.

   1. This list is nested and does not require explicit item continuation.

      This paragraph is part of the preceding list item.

   2. List item b.

   This paragraph belongs to item two of the outer list.

